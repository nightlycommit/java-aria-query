import {mkdir, mkdirSync, readFile, writeFile} from "fs";
import {dirname, join} from "path";

const baseDir = 'src/main/java/com/nightlycommit/aria/roles/etc';

const literalRequires = [];
const abstractRequires = [];
const dpubRequires = [];
const graphicsRequires = [];

function createCamelName(name) {
  const nameArr = name.split('-');
  nameArr[0] = nameArr[0].slice(0, 1).toUpperCase() + nameArr[0].slice(1);

  if (nameArr.length > 1) {
    nameArr[1] = nameArr[1].slice(0, 1).toUpperCase()
      + nameArr[1].slice(1);
  }

  return nameArr.join('') + 'Role';
}

function createKeyName(key) {
  if (key.includes('-')) {
    return `'${key}'`;
  } else {
    return key;
  }
}

function getTypeof(value) {
  return Array.isArray(value)
    ? 'array'
    : (value == null)
      ? 'null'
      : typeof value;
}

function stringifyBoolean(value) {
  return [(value) ? 'true' : 'false'];
}

function stringifyArray(arr, depth) {
  const output = [
    `new String[]{`
  ];

  let tmp = [];

  (new Set(arr)).forEach(
    value => {
      if (value != null) {
        tmp.push(`${constructIndent(depth)}${triageValue(value, (depth))},`);
      }
    }
  );

  output.push(tmp.join(''));
  output.push(`${constructIndent(depth - 1)}`);
  output.push(`}`);

  return output.join('\n');
}

function stringifyObject(value, depth) {
  let output = [];
  const keys = Object.keys(value);
  if (keys.length === 0) {
    output.push('{}');
  } else {
    output.push(`{`);
    const tmp = [];
    keys.forEach(key => {
      tmp.push(`${constructIndent(depth)}${createKeyName(key)}: ${triageValue(value[key], (depth))},`);
    });
    output.push(tmp.join('\n'));
    output.push(`${constructIndent(depth - 1)}}`);
  }
  return output.join('\n');
}

function constructIndent(depth) {
  return Array(depth).fill('  ').join('');
}

/**
 * @typedef Attribute
 * @property {Array<string>} [constraints]
 * @property {string} name
 * @property {string} value
 */

/**
 * @typedef Concept
 * @property {string} name
 * @property {Array<Attribute>} [attributes]
 * @property {Array<string>} [constraints]
 */

/**
 * @typedef RelatedConcept
 * @property {Concept} concept
 * @property {string} module
 */

/**
 * @param {string} constraint
 * @return string
 */
const generateAttributeConstraint = (constraint) => {
  switch (constraint) {
    case "undefined": {
      return 'ARIARoleRelationConceptAttributeConstraint.UNDEFINED';
    }

    case "set": {
      return 'ARIARoleRelationConceptAttributeConstraint.SET';
    }

    case ">1": {
      return 'ARIARoleRelationConceptAttributeConstraint.MORE_THAN_ONE';
    }
  }
}

/**
 * @param attribute {Attribute}
 */
const generateARIARoleRelationConceptAttribute = (attribute) => {
  const constraints = attribute.constraints ? `new ARIARoleRelationConceptAttributeConstraint[] {
    ${attribute.constraints?.map(generateAttributeConstraint).join(',\n')}
}` : null;

  const value = attribute.value ? `"${attribute.value}"` : null;

  return `new ARIARoleRelationConceptAttribute(
  "${attribute.name}", 
  ${value},
  ${constraints}
)`;
}

/**
 * @param {string} value
 */
const generateARIARoleRelationConceptConstraint = (value) => {
  switch (value) {
    case  'scoped to the body element': {
      value = 'SCOPED_TO_THE_BODY_ELEMENT';
      break;
    }

    case  'scoped to the main element': {
      value = 'SCOPED_TO_THE_MAIN_ELEMENT';
      break;
    }
    case  'scoped to a sectioning root element other than body': {
      value = 'SCOPED_TO_A_SECTIONING_ROOT_ELEMENT_OTHER_THAN_BODY';
      break;
    }
    case  'scoped to a sectioning content element': {
      value = 'SCOPED_TO_A_SECTIONING_CONTENT_ELEMENT';
      break;
    }
    case  'direct descendant of document': {
      value = 'DIRECT_DESCENDANT_OF_DOCUMENT';
      break;
    }
    case  'direct descendant of ol': {
      value = 'DIRECT_DESCENDANT_OF_OL';
      break;
    }
    case  'direct descendant of ul': {
      value = 'DIRECT_DESCENDANT_OF_UL';
      break;
    }
    case  'direct descendant of menu': {
      value = 'DIRECT_DESCENDANT_OF_MENU';
      break;
    }
    case  'direct descendant of details element with the open attribute defined': {
      value = 'DIRECT_DESCENDANT_OF_DETAILS_ELEMENT_WITH_THE_OPEN_ATTRIBUTE_DEFINED';
      break;
    }
    case  'ancestor table element has table role': {
      value = 'ANCESTOR_TABLE_ELEMENT_HAS_TABLE_ROLE';
      break;
    }
    case  'ancestor table element has grid role': {
      value = 'ANCESTOR_TABLE_ELEMENT_HAS_GRID_ROLE';
      break;
    }
    case  'ancestor table element has treegrid role': {
      value = 'ANCESTOR_TABLE_ELEMENT_HAS_TREEGRID_ROLE';
      break;
    }
    case  'the size attribute value is greater than 1': {
      value = 'THE_SIZE_ATTRIBUTE_VALUE_IS_GREATER_THAN_1';
      break;
    }
    case  'the multiple attribute is not set and the size attribute does not have a value greater than 1': {
      value = 'THE_MULTIPLE_ATTRIBUTE_IS_NOT_SET_AND_THE_SIZE_ATTRIBUTE_DOES_NOT_HAVE_A_VALUE_GREATER_THAN_1';
      break;
    }
    case  'the list attribute is not set': {
      value = 'THE_LIST_ATTRIBUTE_IS_NOT_SET';
      break;
    }
  }

  return `ARIARoleRelationConceptConstraint.${value}`;
}

/**
 * @param {Concept} concept
 * @returns string
 */
const generateARIARoleRelationConcept = (concept) => {
  const name = concept.name;
  const attributes = concept.attributes ? `new ARIARoleRelationConceptAttribute[] {
  ${concept.attributes.map(generateARIARoleRelationConceptAttribute).join(',\n')}
}` : `null`;
  const constraints = concept.constraints ? `new ARIARoleRelationConceptConstraint[] {
  ${concept.constraints.map(generateARIARoleRelationConceptConstraint).join(',\n')}
}` : `null`;

  return `new ARIARoleRelationConcept(
  "${name}", 
  ${attributes}, 
  ${constraints}
)`;
}

/**
 * @param {string} module
 */
const generateModule = (module) => {
  const resolve = () => {
    switch (module) {
      case "DAISY Guide": {
        return `DAISY_GUIDE`;
      }

      case "Dublin Core": {
        return `DUBLIN_CORE`;
      }

      case "XForms": {
        return `XFORMS`;
      }

      default: {
        return module
      }
    }
  };

  return module ? `ARIAModule.${resolve()}` : 'null';
}

/**
 * @param values {Array<RelatedConcept>}
 */
const createConcepts = (values) => {
  return `new ARIARoleRelation[] {
  ${values.map(({concept, module}) => {
    if (concept) {
      return `new ARIARoleRelation(
  ${generateModule(module)},
  ${generateARIARoleRelationConcept(concept)}
)`
    } else {
      return `new ARIARoleRelation(
  ${generateModule(module)}
)`
    }
  })}
}`;
};

/**
 * @param {Array<string>} values
 */
const createNameFrom = (values) => {
  return `new ARIANameFromSource[] {
  ${values.map((value) => {
    return `ARIANameFromSource.${value.toUpperCase()}`;
  }).join(',\n')}
}`;
};

/**
 * @param {Array<string>} values
 */
const createStringList = (values) => {
  return `new String[] {${values.map((value) => {
    return `"${value}"`
  }).join(', ')}}`;
};

/**
 * @param {Array<Array<string>>} values
 */
const createSuperClasses = (values) => {
  return `new String[][] {
  ${values.map((subValues) => {
    return `new String[] {
    ${subValues.map((value) => `"${value}"`).join(',\n')}
  }`;
  }).join(',\n')}
}`;
};

/**
 * @param {Array<string | [string, string]>} values
 * @returns {string}
 */
const createPropertyMap = (values) => {
  return `new ARIAPropertyMap[] {
  ${values.map((value) => {
    if (typeof value === "string") {
      return `new ARIAPropertyMap("${value}")`;
    } else {
      return `new ARIAPropertyMap("${value[0]}", "${value[1]}")`;
    }
  }).join(',\n')}
}`;
};

/**
 * @param {Array<Array<string>>} elements
 */
const createRequiredOwnedElements = (elements) => {
  return `new String[][] {
  ${elements.map((subElements) => {
    return `new String[] {
    ${subElements.map((subElement) => {
      return `"${subElement}"`;
    }).join(',\n')}
  }`
  }).join(',\n')}
}`
};

function triageValue(value, depth = 0) {
  let output = [];
  switch (getTypeof(value)) {
    case 'object':
      output = output.concat(stringifyObject(value, (depth + 1)));
      break;
    case 'array':
      output = output.concat(stringifyArray(value, (depth + 1)));
      break;
    case 'boolean':
      output = output.concat(stringifyBoolean(value, (depth + 1)));
      break;
    case 'string':
      output = output.concat(`"${value}"`);
      break;
    case 'number':
      output = output.concat(`${value}`);
      break;
    default:
      output.push('null');
  }
  return output;
}

readFile(join('scripts/roles.json'), {
  encoding: 'utf8'
}, (error, data) => {
  if (error) {
    throw error;
  }

  let aria = JSON.parse(data);

  /**
   * @param klasses
   * @param stack
   * @returns {Array<Array<string>>}
   */
  function superClassWalker(klasses, stack = []) {
    let output = [];
    for (let klass of klasses) {
      const newStack = stack.slice();
      newStack.unshift(klass);
      if (aria[klass] === undefined) {
        throw new TypeError(`Missing role '${klass}'`);
      }
      let superClasses = aria[klass]['superClass'];
      if (superClasses.length > 0) {
        output = output.concat(
          superClassWalker(superClasses, newStack)
        );
      } else {
        output.push(newStack);
      }
    }
    return output;
  }

  /**
   * Get a map of superClasses
   *
   * @type {Map<string, Array<Array<string>>>}
   */
  const accumulatedSuperClasses = new Map([]);

  Object.keys(aria).forEach((name) => {
    // Create a set of all the props of the super classes.
    const superClasses = aria[name]['superClass'];
    const accumulation = superClassWalker(superClasses);

    accumulatedSuperClasses.set(name, accumulation);
  });

  Object.keys(aria)
    .forEach((name) => {
      const camelName = createCamelName(name);
      // Deprecate requireContextRole prop. Replace with requiredContextRole.
      // requireContextRole will be removed in a future version.
      aria[name]['requireContextRole'] = aria[name]['requiredContextRole'];
      // Deprecate baseConcepts prop.
      // baseConcepts will be removed in a future version.
      aria[name]['baseConcepts'] = [];

      let subDir = '';

      if (aria[name].abstract) {
        subDir = 'abstractRoles';
        abstractRequires.push(camelName);
      } else if (name.startsWith('doc-')) {
        subDir = 'dpubRoles';
        dpubRequires.push(camelName);
      } else if (name.startsWith('graphics-')) {
        subDir = 'graphicsRoles';
        graphicsRequires.push([name, camelName]);
      } else {
        subDir = 'literalRoles';
        literalRequires.push(camelName);
      }

      const inheritedProps = new Set([]);

      let superClasses = aria[name]['superClass'];

      while (superClasses.length > 0) {
        const cloned = superClasses.slice();

        superClasses = [];

        cloned.forEach((superClass) => {
          aria[superClass]['props'].concat(aria[superClass]['requiredProps']).forEach(
            superProp => {
              const name = (Array.isArray(superProp))
                ? superProp[0]
                : superProp;
              inheritedProps.add(name);
            }
          );

          superClasses = superClasses.concat(aria[superClass]['superClass']);
        });
      }
      // Create a map of all prop names
      const propNamesMap = aria[name]['props'].reduce((acc, item, index) => {
        const pName = Array.isArray(item) ? item[0] : item;
        acc[pName] = index;
        return acc;
      }, {});
      // Add the required props for this role to the props list.
      aria[name]['requiredProps'].forEach((rProp) => {
        const rName = Array.isArray(rProp) ? rProp[0] : rProp;
        // If the prop exists in the list of props, replace it. Otherwise, just add it.
        if (propNamesMap[rName] >= 0) {
          aria[name]['props'].splice(propNamesMap[rName], 1, rProp);
        } else {
          aria[name]['props'].push(rProp);
        }
      });
      // Remove the props that exist in the super classes unless the
      // prop has a default value.
      aria[name]['props'] = aria[name]['props'].filter(
        prop => {
          let keep = true;
          if (
            !Array.isArray(prop)
            && inheritedProps.has(prop)
          ) {
            keep = false;
          }
          return keep;
        });

      const code = `package com.nightlycommit.aria.roles.etc.${subDir}; 

import com.nightlycommit.aria.roles.*;

public class ${camelName} {
  private ${camelName}() {}

  public static final ARIARoleDefinition definition = new ARIARoleDefinition(
    "${name}",
    ${aria[name]['abstract']},
    ${aria[name]['accessibleNameRequired']},
    ${createConcepts(aria[name]['baseConcepts'])},
    ${aria[name]['childrenPresentational']},
    ${createNameFrom(aria[name]['nameFrom'])},
    ${createStringList(aria[name]['prohibitedProps'])},
    ${createPropertyMap(aria[name]['props'])},
    ${createConcepts(aria[name]['relatedConcepts'])},
    ${createStringList(aria[name]['requiredContextRole'])},
    ${createRequiredOwnedElements(aria[name]['requiredOwnedElements'])},
    ${createPropertyMap(aria[name]['requiredProps'])},
    ${createSuperClasses(accumulatedSuperClasses.get(name))}
  );
}`;
//         Object.keys(aria[name])
//           .sort()
//           .filter((prop) => {
//             if (prop === 'props') {
//               // Create a set of all the props of the super classes.
//               const inheritedProps = new Set([]);
//               let superClasses = aria[name]['superClass'];
//               while (superClasses.length > 0) {
//                 const cloned = superClasses.slice();
//                 superClasses = [];
//                 cloned.forEach(superClass => {
//                   aria[superClass]['props'].concat(aria[superClass]['requiredProps']).forEach(
//                     superProp => {
//                       const name = (Array.isArray(superProp))
//                         ? superProp[0]
//                         : superProp;
//                       inheritedProps.add(name);
//                     }
//                   );
//                   superClasses = superClasses.concat(aria[superClass]['superClass']);
//                 });
//               }
//               // Create a map of all prop names
//               const propNamesMap = aria[name]['props'].reduce((acc, item, index) => {
//                 const pName = Array.isArray(item) ? item[0] : item;
//                 acc[pName] = index;
//                 return acc;
//               }, {});
//               // Add the required props for this role to the props list.
//               aria[name]['requiredProps'].forEach((rProp) => {
//                 const rName = Array.isArray(rProp) ? rProp[0] : rProp;
//                 // If the prop exists in the list of props, replace it. Otherwise, just add it.
//                 if (propNamesMap[rName] >= 0) {
//                   aria[name]['props'].splice(propNamesMap[rName], 1, rProp);
//                 } else {
//                   aria[name]['props'].push(rProp);
//                 }
//               });
//               // Remove the props that exist in the super classes unless the
//               // prop has a default value.
//               aria[name]['props'] = aria[name]['props'].filter(
//                 prop => {
//                   let keep = true;
//                   if (
//                     !Array.isArray(prop)
//                     && inheritedProps.has(prop)
//                   ) {
//                     keep = false;
//                   }
//                   return keep;
//                 });
//             }
//             // Always returns true. Using filter simply to chain.
//             return true;
//           })
//           .map((prop) => {
//             let value = aria[name][prop];
//
//             if (prop === 'relatedConcepts') {
//               return createConcepts(value);
//             }
//
//             if (['props', 'requiredProps'].includes(prop)) {
//               value = value.reduce((acc, item) => {
//                 let name;
//                 let defaultVal;
//                 if (Array.isArray(item)) {
//                   name = item[0];
//                   defaultVal = item[1];
//                 } else {
//                   name = item;
//                   defaultVal = null;
//                 }
//                 acc[name] = defaultVal;
//                 return acc;
//               }, {});
//             }
//             if (prop === 'superClass') {
//               value = accumulatedSuperClasses.get(name);
//             }
//             let depth = 1;
//             return `${triageValue(value, depth).join('')}`;
//           }).join(',\n'),
//         `
//   );
// }
// `,

      const filePath = join(baseDir, subDir, `${camelName}.java`);

      mkdirSync(dirname(filePath), {
        recursive: true
      });

      writeFile(
        filePath,
        code,
        {
          encoding: 'utf8'
        },
        (error) => {
          if (error) {
            throw error;
          }

          console.log(`Created file ${baseDir}/${subDir}/${camelName}.java`);
        });
    });
});

readFile(join('scripts/roles.json'), {
  encoding: 'utf8'
}, (error, data) => {
  if (error) {
    throw error;
  }

  const literalRequires = [];
  const abstractRequires = [];
  const dpubRequires = [];
  const graphicsRequires = [];

  const aria = JSON.parse(data);

  Object.keys(aria)
    .forEach((name) => {
      const camelName = createCamelName(name);

      if (aria[name].abstract) {
        abstractRequires.push([name, camelName]);
      } else if (name.startsWith('doc-')) {
        dpubRequires.push([name, camelName]);
      } else if (name.startsWith('graphics-')) {
        graphicsRequires.push([name, camelName]);
      } else {
        literalRequires.push([name, camelName]);
      }
    });

  const roleDefinitions = [];
  const roleDefinitionImports = [];
  const roles = [];

  for (const [roles, name] of [
    [literalRequires, 'literalRoles'],
    [abstractRequires, 'abstractRoles'],
    [dpubRequires, 'dpubRoles'],
    [graphicsRequires, 'graphicsRoles'],
  ]) {
    roleDefinitionImports.push(`
${roles.map((role) => {
      return `import com.nightlycommit.aria.roles.etc.${name}.${role[1]};`;
    }).join('\n')}
`);

    roleDefinitions.push(`public static ARIARoleDefinition[] ${name} = new ARIARoleDefinition[] {
  ${roles.map((role) => {
      return `${role[1]}.definition`;
    }).join(',\n')}
};
`);
  }

  const code = `package com.nightlycommit.aria.roles.etc;

import com.nightlycommit.aria.roles.ARIARoleDefinition;
${roleDefinitionImports.join('\n')}

public class Index {
  private Index() {}
 
  ${roleDefinitions.join('\n')}
}`;

  const filePath = join(baseDir, `Index.java`);

  mkdirSync(dirname(filePath), {
    recursive: true
  });

  writeFile(
    filePath,
    code,
    {
      encoding: 'ascii'
    },
    (error) => {
      if (error) {
        throw error;
      }

      console.log(`Created file ${baseDir}/ARIARolesModel.java`);
    }
  );
});