package com.nightlycommit.aria.roles;

public class ARIARoleRelationConceptAttribute {
  public final String name;
  public final String value;
  public final ARIARoleRelationConceptAttributeConstraint[] constraints;

  public ARIARoleRelationConceptAttribute(
    String name,
    String value,
    ARIARoleRelationConceptAttributeConstraint[] constraints
  ) {
    this.name = name;
    this.value = value;
    this.constraints = constraints;
  }
}
