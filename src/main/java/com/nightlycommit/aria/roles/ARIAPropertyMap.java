package com.nightlycommit.aria.roles;

public class ARIAPropertyMap {
  public final String name;

  public final String value;

  public ARIAPropertyMap(String name) {
    this.name = name;
    this.value = null;
  }

  public ARIAPropertyMap(String name, String value) {
    this.name = name;
    this.value = value;
  }
}
