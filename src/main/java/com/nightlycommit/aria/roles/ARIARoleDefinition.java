package com.nightlycommit.aria.roles;

public class ARIARoleDefinition {
  public final String name;
  public final Boolean isAbstract;
  public final Boolean accessibleNameIsRequired;
  public final ARIARoleRelation[] baseConcepts;
  public final Boolean isChildrenPresentational;
  public final ARIANameFromSource[] nameFrom;
  public final String[] prohibitedProps;
  public final ARIAPropertyMap[] props;
  public final ARIARoleRelation[] relatedConcepts;
  public final String[] requiredContextRole;
  public final String[][] requiredOwnedElements;
  public final ARIAPropertyMap[] requiredProps;

  /* An array or super class "stacks." Each stack contains a LIFO list of
   * strings correspond to a super class in the inheritance chain of this
   * role. Roles may have more than one inheritance chain, which is why
   * this property is an array of arrays and not a single array. */
  public final String[][] superClass;

  public ARIARoleDefinition(
    String name,
    Boolean isAbstract,
    Boolean accessibleNameRequired,
    ARIARoleRelation[] baseConcepts,
    Boolean childrenPresentational,
    ARIANameFromSource[] nameFrom,
    String[] prohibitedProps,
    ARIAPropertyMap[] props,
    ARIARoleRelation[] relatedConcepts,
    String[] requiredContextRole,
    String[][] requiredOwnedElements,
    ARIAPropertyMap[] requiredProps,
    String[][] superClass
  ) {
    this.name = name;
    this.isAbstract = isAbstract;
    this.accessibleNameIsRequired = accessibleNameRequired;
    this.baseConcepts = baseConcepts;
    this.isChildrenPresentational = childrenPresentational;
    this.nameFrom = nameFrom;
    this.prohibitedProps = prohibitedProps;
    this.props = props;
    this.relatedConcepts = relatedConcepts;
    this.requiredContextRole = requiredContextRole;
    this.requiredOwnedElements = requiredOwnedElements;
    this.requiredProps = requiredProps;
    this.superClass = superClass;
  }
}
