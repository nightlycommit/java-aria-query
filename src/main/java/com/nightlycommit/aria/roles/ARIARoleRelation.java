package com.nightlycommit.aria.roles;

public class ARIARoleRelation {
  public final ARIAModule module;
  public final ARIARoleRelationConcept concept;

  public ARIARoleRelation(
    ARIAModule module
  ) {
    this.module = module;
    this.concept = null;
  }

  public ARIARoleRelation(
    ARIAModule module,
    ARIARoleRelationConcept concept
  ) {
    this.module = module;
    this.concept = concept;
  }
}
