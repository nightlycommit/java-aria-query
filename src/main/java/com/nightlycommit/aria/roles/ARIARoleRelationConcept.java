package com.nightlycommit.aria.roles;

public class ARIARoleRelationConcept {
  public final String name;
  public final ARIARoleRelationConceptAttribute[] attributes;
  public final ARIARoleRelationConceptConstraint[] constraints;

  public ARIARoleRelationConcept(
    String name
  ) {
    this.name = name;
    this.attributes = null;
    this.constraints = null;
  }

  public ARIARoleRelationConcept(
    String name,
    ARIARoleRelationConceptAttribute[] attributes,
    ARIARoleRelationConceptConstraint[] constraints
  ) {
    this.name = name;
    this.attributes = attributes;
    this.constraints = constraints;
  }
}
