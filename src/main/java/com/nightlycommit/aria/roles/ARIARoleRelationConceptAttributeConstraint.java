package com.nightlycommit.aria.roles;

public enum ARIARoleRelationConceptAttributeConstraint {
  UNDEFINED,
  SET,
  MORE_THAN_ONE
}
