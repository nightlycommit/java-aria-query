package com.nightlycommit.aria.roles;

public enum ARIANameFromSource {
  AUTHOR,
  CONTENTS,
  PROHIBITED
}
