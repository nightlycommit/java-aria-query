package com.nightlycommit.aria.roles;

public enum ARIAModule {
  ARIA,
  DAISY,
  DAISY_GUIDE,
  DUBLIN_CORE,
  DTB,
  EPUB,
  GRAPHICS,
  HTML,
  JAPI,
  SMIL,
  XFORMS,
  XHTML
}
