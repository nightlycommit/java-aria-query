package com.nightlycommit.aria.roles;

import com.nightlycommit.aria.roles.etc.Index;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Stream;

public class ARIARolesModel {
  private ARIARolesModel() {
  }

  static ARIARoleDefinition[] rolesMap;

  static {
    var roleDefinitions = Stream.of(
      Index.abstractRoles,
      Index.literalRoles,
      Index.dpubRoles,
      Index.graphicsRoles
    ).flatMap(Stream::of).toArray(ARIARoleDefinition[]::new);

    rolesMap = Arrays.stream(roleDefinitions).map((roleDefinition) -> {
      var props = new HashSet<>(Arrays.stream(roleDefinition.props).toList());

      for (var superClasses : roleDefinition.superClass) {
        for (var superClassName : superClasses) {
          var superClassRole = Arrays.stream(roleDefinitions)
            .filter((candidate) -> {
              return candidate.name.equals(superClassName);
            })
            .findFirst()
            .orElse(null);

          if (superClassRole != null) {
            props.addAll(Arrays.stream(superClassRole.props).toList());
          }
        }
      }

      return new ARIARoleDefinition(
        roleDefinition.name,
        roleDefinition.isAbstract,
        roleDefinition.accessibleNameIsRequired,
        roleDefinition.baseConcepts,
        roleDefinition.isChildrenPresentational,
        roleDefinition.nameFrom,
        roleDefinition.prohibitedProps,
        props.toArray(ARIAPropertyMap[]::new),
        roleDefinition.relatedConcepts,
        roleDefinition.requiredContextRole,
        roleDefinition.requiredOwnedElements,
        roleDefinition.requiredProps,
        roleDefinition.superClass
      );
    }).toArray(ARIARoleDefinition[]::new);
  }

  public static ARIARoleDefinition getRole(String name) {
    return Arrays.stream(rolesMap).filter((roleDefinition) -> {
      return roleDefinition.name.equals(name);
    }).findFirst().orElse(null);
  }
  public static Map<String, ARIARoleRelationConcept[]> roleElements() {
    var result = new HashMap<String, ARIARoleRelationConcept[]>();

    for (var role : rolesMap) {
      var roleRelationConcepts = new ArrayList<ARIARoleRelationConcept>();

      for (var relatedConcept : role.relatedConcepts) {
        if (relatedConcept.module == ARIAModule.HTML) {
          roleRelationConcepts.add(relatedConcept.concept);
        }
      }

      result.put(role.name, roleRelationConcepts.toArray(ARIARoleRelationConcept[]::new));
    }

    return result;
  }
}
