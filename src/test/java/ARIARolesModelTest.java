import com.nightlycommit.aria.roles.ARIARolesModel;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ARIARolesModelTest {
  @Test
  void roleElements() {
    var roleElements = ARIARolesModel.roleElements();

    assertEquals(roleElements.size(), 137);
  }
}
